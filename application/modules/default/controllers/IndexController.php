<?php
	
	class IndexController extends Zend_Controller_Action
	{
		function indexAction()
		{
		}
		
		function newsAction()
		{
			$this->view->title = ': News';
		}
		
		function informationAction()
		{
			$this->view->title = ': Information';
		}
		
		function resourcesAction()
		{
			$this->view->title = ': Resources';
		}
		
		function linksAction()
		{
			$this->view->title = ': Links';
		}
	}
?>