<?php

	class Section
	{
		private $filename;
		private $section;
		
		function __construct($filename, $section)
		{
			$this->filename = strToUpper($filename);
			$this->section  = $section;
		}
		
		function getTitle()
		{
			return (string) $this->section->title;
		}
		
		function getNumber()
		{
			// Don't show the section number for these sections (section number = filename)
			$isValidSectionNumber = (boolean) preg_match("/\d\..*/i", $this->filename);
			
			return ($isValidSectionNumber) ? "[{$this->filename}] " : '';
		}
		
		function getBody()
		{
			return (string) $this->section->body;
		}
		
		function getUpdated()
		{
			return $this->section->updated;
		}
		
		function getPrev()
		{
			return ($this->section->prev == 'null') ? null : $this->section->prev;
		}
		
		function getUp()
		{
			return ($this->section->up == 'null') ? null : $this->section->up;
		}
		
		function getNext()
		{
			return ($this->section->next == 'null') ? null : $this->section->next;
		}
		
		function isIndex()
		{
			return (stristr($this->filename, 'chapter') || strcasecmp($this->filename, 'toc') == 0);
		}
	}
?>