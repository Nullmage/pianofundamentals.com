<?php

	set_time_limit(90);
	
	require_once 'Zend/Search/Lucene.php';
	
	function isValidSection($fileName)
	{
		return !(
			strstr($fileName, 'abbreviations') ||
			strstr($fileName, 'chapter') ||
			strstr($fileName, 'toc')
		);
	}
	
	function makeDocId($filename)
	{
		$newName = basename($filename, '.xml');
		
		$search  = array('1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '.');
		$replace = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', '');
		
		$newName = str_replace($search, $replace, $newName);
		
		return $newName;
	}
	
	$lang = 'en';
	
	// Files to be indexed are located here
	$idxIn = DIR_LANGUAGES . "$lang/book";
	
	// Location where the index will be stored
	$idxOut = DIR_LANGUAGES . "$lang/index/";
	
	$index = Zend_Search_Lucene::create($idxOut);
	
	foreach ($files = glob("$idxIn/*.xml") as $file) {
		if (isValidSection($file)) {
			$doc = new Zend_Search_Lucene_Document();
			
			$section = simplexml_load_file($file);

			$doc->addField(Zend_Search_Lucene_Field::Text('docid', makeDocId($file)));
			$doc->addField(Zend_Search_Lucene_Field::Keyword('filename', basename($file, '.xml')));
			$doc->addField(Zend_Search_Lucene_Field::Text('title', (string) $section->title, 'utf-8'));
			$doc->addField(Zend_Search_Lucene_Field::UnStored('body', strip_tags((string) $section->body), 'utf-8'));
			
			$index->addDocument($doc);
		}
	}
	
	$index->optimize();
?>