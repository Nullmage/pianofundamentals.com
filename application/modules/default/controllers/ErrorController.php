<?php
	
	class ErrorController extends Zend_Controller_Action
	{
		function errorAction()
		{
			// Treat redirects to this action as a 404 error
			if (!$this->_hasParam('error_handler')) {
				return $this->_forward('error404');
			}
			
			$error = $this->_getParam('error_handler');
			
			switch ($error->type) {
				case 'EXCEPTION_NO_CONTROLLER':
				case 'EXCEPTION_NO_ACTION':
					return $this->_forward('error404');
				
				// Only unhandled exceptions get caught (and logged) here
				case 'EXCEPTION_OTHER':
					$e = $error->exception;
					
					$errMsg  = "Unhandled Application Exception: ";
					$errMsg .= "Message: \"{$e->getMessage()}\" ";
					$errMsg .= "Type: " . get_class($e) . " ";
					$errMsg .= "File: {$e->getFile()} (Line: {$e->getLine()}) ";
					$errMsg .= "Trace: {$e->getTraceAsString()}";
					
					error_log($errMsg);
					
					return $this->_forward('error500');
			}
		}
		
		function error404Action()
		{
			$this->view->title = 'Error 404 - File Not Found';
			
			$this->getResponse()->setRawHeader('HTTP/1.1 404 Not Found');
		}
		
		function error500Action()
		{
			$this->view->title = 'Error 500 - Internal Server Error';
			
			$this->getResponse()->setRawHeader('HTTP/1.1 500 Internal Server Error');
		}
	}
?>