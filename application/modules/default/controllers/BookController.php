<?php
	
	class BookController extends Zend_Controller_Action
	{
		function indexAction()
		{
			require_once DIR_DEFAULT_MODULE . 'models/SectionLoader.php';
			
			$loader = new SectionLoader();
			$section = $loader->fetch('toc');
			
			$this->view->title        = ': ' . $section->getTitle();
			$this->view->sectionTitle = $section->getTitle();
			$this->view->body         = $section->getBody();
			$this->view->revisions    = file_get_contents(DIR_LANGUAGES . "en/revisions.phtml"); // akta $lang
		}
		
		function searchAction()
		{
			require_once DIR_DEFAULT_MODULE . 'models/BookSearcher.php';
			
			$keywords = $this->_request->getParam('keywords');
			$option   = $this->_request->getParam('option');
			$lang     = $this->_request->getParam('lang');
			
			$searcher = new BookSearcher($option, $lang);
			
			$this->view->title    = ': Search Results';
			$this->view->keywords = $keywords;
			
			if (!$hits = $searcher->query($keywords)) {
				$this->view->searchError = $searcher->getErrorMsg();
				
				$this->render('search-no-hits');
			}
			
			else {
				if (count($hits) > 0) {
					$this->view->hits = $hits;
					
					$this->render('search-hits');
				}
				
				else {
					$this->render('search-no-hits');
				}
			}
		}
		
		function viewAction()
		{
			require_once DIR_DEFAULT_MODULE . 'models/SectionLoader.php';
			
			$lang = $this->_request->getParam('lang');
			$sect = $this->_request->getParam('section');
			
			$loader = new SectionLoader($lang);
			
			if (!($section = $loader->fetch($sect))) {
				$this->_redirect('/Error/error');
			}
			
			$this->view->title         = ': ' . $section->getTitle();
			$this->view->sectionNumber = $section->getNumber();
			$this->view->sectionTitle  = $section->getTitle();
			$this->view->sectionBody   = $section->getBody();
			
			if ($section->isIndex()) {
				$this->render('toc');
			}
			
			else {
				$this->view->updated = $section->getUpdated();
				$this->view->prev    = $section->getPrev();
				$this->view->up      = $section->getUp();
				$this->view->next    = $section->getNext();
				
				$this->render('section');
			}
		}
	}
	
	/*
		// --- Setup cache ---
		
		require_once 'Zend/Cache.php';

		$backend = 'File';
		$frontend = 'File';
		
		$backendOptions = array(
			'cache_dir' => '../application/var/cached_views/',
			'file_locking' => true,
			'read_control' => true,
			'read_control_type' => 'crc32'
		);
		
		$frontendOptions = array(
			'caching' => true,
			'lifetime' => null,
			'logging' => false,
			'write_control' => true,
			'master_file' => "../application/langs/$lang/book/$sect.xml",
			'automatic_serialization' => true,
			'automatic_cleaning_factor' => 0
		);

		$cache = Zend_Cache::factory($frontend, $backend, $frontendOptions, $backendOptions);
	
		// --- End setup cache ---
		
		// ID of the cache entry
		$id = $lang . '_' . str_replace('.', '', $sect);
		
		if (!($section = $cache->load($id))) {
			if (!is_readable($file)) {
				$this->_redirect('/Error/error');
			}
			
			$section = simplexml_load_file($file);
			
			// Convert section to array, stringify the content node, then convert the
			// result back to an object, otherwise unserialize will complain about 
			// "Node no longer exists..." when we fetch it from the cache.
			$tmp = (array) $section;
			$tmp['content'] = (string) $tmp['content'];
			
			$cache->save((object) $tmp, $id);
		}
	*/
?>