function setCookie(name, value, expires, path, domain, secure)
{
	if (expires)
	{
		var date = new Date();
		date.setTime(date.getTime() + (expires * 86400000)); // 24 * 60 * 60 * 1000
		expires = date.toGMTString();
	}
	
	document.cookie =
		name + " = " + escape(value) +
		((expires) ? "; expires=" + expires : "") +
		((path)    ? "; path=" + path : "") +
		((domain)  ? "; domain=" + domain : "") +
		((secure)  ? "; secure" : "");
}

function getCookie(name)
{
	var dc = document.cookie;
	var prefix = name + "=";
	var begin = dc.indexOf("; " + prefix);
	
	if (begin == -1)
	{
		begin = dc.indexOf(prefix);
		
		if (begin != 0)
			return null;
	}
	
	else
		begin += 2;
	
	var end = document.cookie.indexOf(";", begin);
	
	if (end == -1)
		end = dc.length;
	
	return unescape(dc.substring(begin + prefix.length, end));
}

function deleteCookie(name)
{
	setCookie(name, "", -1);
}