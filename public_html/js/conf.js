// Adjust font size

fontScale = getCookie("fontscale");

if (fontScale != null)
{
	switch (fontScale)
	{
		case "1":
			fontSize = "80%";
			document.minus.src = "/img/minus_icon_faded.gif";
			break;
		
		case "2":
			fontSize = "90%";
			break;
		
		case "3":
			fontSize = "100%";
			break;
		
		case "4":
			fontSize = "110%";
			break;
		
		case "5":
			fontSize = "120%";
			document.plus.src = "/img/plus_icon_faded.gif";
			break;
	}
	
	paragraphs     = document.getElementsByTagName('p');
	orderedLists   = document.getElementsByTagName('ol');
	
	for (i = 0; i < paragraphs.length; ++i) {
		paragraphs[i].style.fontSize = fontSize;
	}
	
	for (i = 0; i < orderedLists.length; ++i) {
		orderedLists[i].style.fontSize = fontSize;
	}
}

// Show/Hide panel

panel = getCookie("panel");

if (panel != null)
{
	sideBarId = "sidebar";
	contentId = "content";
	showBarId = "showSidebar";
	
	if (document.getElementById) // DOM3 = IE5, NS6
	{
		if (document.getElementById(contentId) != null)
		{
			document.getElementById(sideBarId).style.display = (panel === "true") ? 'block' : 'none';
			document.getElementById(contentId).style.width   = (panel === "true") ? '483px' : 'auto';
			document.getElementById(showBarId).style.display = (panel === "true") ? 'none'  : 'block';
		}
	}
}
