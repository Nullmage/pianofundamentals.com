function resizeFont(up)
{
	// First time we adjust font size?
	if (undefined === window.size)
	{
		size     = 3; // size will vary from 1 - 5
		fontSize = 100;
	}
	
	// Size up
	if (up && size < 5)
	{
		++size;
		fontSize += 10;
	}
	
	// Size down
	else if (!up && size > 1)
	{
		--size;
		fontSize -= 10;
	}
	
	// Change Plus icon
	if (size == 5)
		document.plus.src = "/img/plus_icon_faded.gif";
	
	// Change Minus icon
	else if (size == 1)
		document.minus.src = "/img/minus_icon_faded.gif";
	
	// Reset both pictures
	else
	{
		document.plus.src  = "/img/plus_icon.gif";
		document.minus.src = "/img/minus_icon.gif";
	}
	
	paragraphs     = document.getElementsByTagName('p');
	orderedlists   = document.getElementsByTagName('ol');
	unorderedLists = document.getElementsByTagName('ul');
	
	// Apply font changes
	for (i = 0; i < paragraphs.length; ++i)
		paragraphs[i].style.fontSize = fontSize + "%";
	
	for (i = 0; i < orderedlists.length; ++i)
		orderedlists[i].style.fontSize = fontSize + "%";
	
	// Save font size settings for a week
	setCookie("fontscale", size, 7);
}

function drawSidebar(flag)
{
	sideBarId = "sidebar";
	contentId = "content";
	showBarId = "showSidebar";
	adRightId = "adRight";
	
	if (document.getElementById) // DOM3 = IE5, NS6
	{
		document.getElementById(sideBarId).style.display = (flag) ? 'block' : 'none';
		document.getElementById(contentId).style.width   = (flag) ? '483px' : 'auto';
		document.getElementById(showBarId).style.display = (flag) ? 'none'  : 'block';
		document.getElementById(adRightId).style.display = (flag) ? 'block' : 'none';
	}
	
	// Save panel settings for a week
	setCookie("panel", flag, 7);
}
